package fr.frivec.skyfall;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.Location;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldedit.math.BlockVector3;

import fr.frivec.api.core.logger.Logger.LogLevel;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.core.servers.status.ServerStatus;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.cuboid.Cuboid;
import fr.frivec.api.spigot.scoreboard.ScoreboardPattern;
import fr.frivec.api.spigot.teams.TeamManager;
import fr.frivec.core.Credentials;
import fr.frivec.core.redis.Redis;
import fr.frivec.skyfall.commands.admin.StartCommand;
import fr.frivec.skyfall.commands.dev.DevSkyFallCommand;
import fr.frivec.skyfall.listeners.chest.ChestOpeningListener;
import fr.frivec.skyfall.listeners.inventory.InventoryListener;
import fr.frivec.skyfall.listeners.players.damages.PlayerDamageListener;
import fr.frivec.skyfall.listeners.players.movement.PlayerMoveListener;
import fr.frivec.skyfall.listeners.players.spectator.SpectatorActionsListener;
import fr.frivec.skyfall.loots.ChestLoots;
import fr.frivec.skyfall.managers.PlayerManager;
import fr.frivec.skyfall.player.SkyfallPlayer;
import fr.frivec.skyfall.runnables.GameRunnable;
import fr.frivec.skyfall.runnables.LobbyRunnable;
import fr.frivec.skyfall.schematics.SchematicLoader;
import fr.frivec.skyfall.utils.States;
import fr.frivec.skyfall.world.WorldBuilder;
import fr.frivec.skyfall.world.WorldDestroyer;

public class SkyFall extends JavaPlugin {
	
	/*
	 * Bugs remarqués
	 * 
	 * - Timer bug si start rapide par vote et start admin sont lancés en même temps
	 * - Timer admin ne peut pas être lancé si vote strat déjà actif
	 * - Spectateurs peuvent ouvrir coffres
	 * 
	 * 
	 */
	
	private static SkyFall instance;
	private static SpigotAPI api;
	
	private Redis redis;
	
	/*
	 * Useful informations
	 */
	private Location mapCenter;
	private States states;
	private HashSet<SkyfallPlayer> players;
	private int amountPlayers;
	private BossBar bossBar;
	private TeamManager teamManager;
	
	/*
	 * Runnables 
	 */
	private LobbyRunnable lobbyRunnable;
	private GameRunnable gameRunnable;
	
	/*
	 * Scoreboard Patterns
	 */
	private ScoreboardPattern lobbyPattern,
								gamePattern;
	
	/*
	 * Loots
	 */
	private ChestLoots loots;
	private WorldBuilder worldBuilder;
	private WorldDestroyer worldDestroyer;
	
	/*
	 * Schematics
	 */
	private Path schematicFolder;
	private SchematicLoader schemLoader;
	private List<String> schematics;
	
	@Override
	public void onEnable() {
		
		instance = this;
		api = SpigotAPI.getInstance();
		
		/*
		 * Managers
		 */
		
		this.loots = new ChestLoots();
		this.worldBuilder = new WorldBuilder();
		this.worldDestroyer = new WorldDestroyer(this.worldBuilder);
		this.redis = new Redis(new Credentials("localhost", "HUB", "5QB?CLmq;Z3]4zrB&dS832H9#ix%#xw8r&R6[A{=z9]t8Eh|HW%8U6r3", 6379));
		
		this.setStates(States.WAIT);
		this.players = new HashSet<>();
		
		getApi().getManagerController().registerManager(new PlayerManager());
		
		/*
		 * Set join and quit messages
		 */
		
		for(RankList ranks : RankList.values()) {
		
			getApi().getConnectionMessageListener().getConnectionMessages().setJoinMessage(ranks, "");
			getApi().getConnectionMessageListener().getConnectionMessages().setQuitMessage(ranks, "");
			
		}
		
		/*
		 * Runnables
		 */
		
		this.gameRunnable = new GameRunnable();
		
		/*
		 * Create scoreboard patterns
		 */
		this.lobbyPattern = new ScoreboardPattern("§6§lSkyFall");
		this.lobbyPattern.setLine(0, "§g");
		this.lobbyPattern.setLine(1, "§7-----------------");
		this.lobbyPattern.setLine(2, "§e");
		this.lobbyPattern.setLine(3, "§9Statut: En attente");
		this.lobbyPattern.setLine(4, "§d");
		this.lobbyPattern.setLine(5, "§6Joueurs en jeu: §f0");
		this.lobbyPattern.setLine(6, "§a");
		this.lobbyPattern.setLine(7, "§7-----------------");
		
		this.gamePattern = new ScoreboardPattern("§6§l");
		this.lobbyPattern.setLine(0, "§g");
		this.gamePattern.setLine(1, "§7--------------");
		this.gamePattern.setLine(2, "§9Serveur ➭ §fnull");
		this.gamePattern.setLine(3, "§r ");
		this.gamePattern.setLine(4, "§5Mode ➭ §fSolo");
		this.gamePattern.setLine(5, "§8 ");
		this.gamePattern.setLine(6, "§6Joueur(s) en vie ➭ §f" + getPlayers().size());
		this.gamePattern.setLine(7, "§b ");
		this.gamePattern.setLine(8, "§aTué(s) ➭  §f0");
		this.gamePattern.setLine(9, "§c ");
		this.gamePattern.setLine(10, "§dCentre ➭ §f0.0");
		this.gamePattern.setLine(11, "§l ");
		this.gamePattern.setLine(12, "§4Durée ➭ §f" + new SimpleDateFormat("mm:ss").format(getGameRunnable().getTimer() * 1000));
		this.gamePattern.setLine(13, "§7--------------");
		
		/*
		 * Create schematics folder
		 */
			
		try {
			
			if(Files.notExists(this.getDataFolder().toPath()))
				
				Files.createDirectory(this.getDataFolder().toPath());
			
			this.schematicFolder = Paths.get(this.getDataFolder() + "/schematics/");
			
			if(Files.notExists(this.schematicFolder))
			
				Files.createDirectory(this.schematicFolder);

		} catch (IOException e) {
			
			SpigotAPI.log(LogLevel.SEVERE, "Unable to create schematics' folder.");
			e.printStackTrace();
				
			this.getServer().shutdown();
			
		}
		
		this.schemLoader = new SchematicLoader(this.schematicFolder);
		this.schematics = Arrays.asList("Jungle_Island_1", "Jungle_Island_2", "Mesa_Island_1", "Mesa_Island_2", "Ice_Island_1", "Ice_Island_2", "Desert_Island_1", "Desert_Island_2");
		
		/*
		 * Register center island
		 */
		
		getApi().updateServerStatus(ServerStatus.MAP_LOADING);
		
		this.mapCenter = new Location(Bukkit.getWorld("world"), 0, 0, 0);
		
		this.worldBuilder.registerCenterIsland(this.mapCenter, new Cuboid(this.mapCenter.clone().add(10, 10, 10), this.mapCenter.clone().subtract(10, 10, 10)));
		
		/*
		 * Build world
		 */
		
		this.worldBuilder.runTaskTimer(SkyFall.getInstance(), 0L, 10L);
		SpigotAPI.log(LogLevel.INFO, "§3Starting world generation");
		
		/*
		 * Commands
		 */
		
		new DevSkyFallCommand();
		new StartCommand();
		
		/*
		 * Listeners
		 */
		getApi().getListenerManager().registerListener(new ChestOpeningListener());
		getApi().getListenerManager().registerListener(new PlayerMoveListener());
		getApi().getListenerManager().registerListener(new InventoryListener());
		getApi().getListenerManager().registerListener(new PlayerDamageListener());
		getApi().getListenerManager().registerListener(new SpectatorActionsListener());
		
		/*
		 * Gamerule
		 */
		getServer().getWorld("world").setGameRule(GameRule.DO_MOB_SPAWNING, false);
		getServer().getWorld("world").setGameRule(GameRule.DO_IMMEDIATE_RESPAWN, true);
		
		/*
		 * BossBar
		 */
		this.bossBar = Bukkit.getServer().createBossBar("§7En attente de joueurs...", BarColor.YELLOW, BarStyle.SOLID, new BarFlag[] {});
		this.getBossBar().setVisible(true);
		this.getBossBar().setProgress(1.0);
		
		/*
		 * Teams
		 */
		this.teamManager = new TeamManager();
		
		super.onEnable();
		
	}
	
	@Override
	public void onDisable() {
		
		super.onDisable();
	}
	
	public static Location convertBlockVector3ToLocation(final BlockVector3 vector) {
		
		return new Location(Bukkit.getWorld("world"), vector.getX(), vector.getY(), vector.getZ());
		
	}
	
	public static SkyFall getInstance() {
		return instance;
	}
	
	public static SpigotAPI getApi() {
		return api;
	}
	
	public Redis getRedis() {
		return redis;
	}
	
	public SchematicLoader getSchemLoader() {
		return schemLoader;
	}
	
	public List<String> getSchematics() {
		return schematics;
	}
	
	public ChestLoots getLoots() {
		return loots;
	}
	
	public WorldBuilder getWorldBuilder() {
		return worldBuilder;
	}
	
	public WorldDestroyer getWorldDestroyer() {
		return worldDestroyer;
	}
	
	public States getCurrentState() {
		return states;
	}
	
	public LobbyRunnable getLobbyRunnable() {
		return lobbyRunnable;
	}
	
	public void setLobbyRunnable(LobbyRunnable lobbyRunnable) {
		this.lobbyRunnable = lobbyRunnable;
	}
	
	public void setStates(States states) {
		this.states = states;
	}
	
	public HashSet<SkyfallPlayer> getPlayers() {
		return players;
	}
	
	public void setBossBar(BossBar bossBar) {
		this.bossBar = bossBar;
	}
	
	public BossBar getBossBar() {
		return bossBar;
	}
	
	public ScoreboardPattern getLobbyPattern() {
		return lobbyPattern;
	}
	
	public void setLobbyPattern(ScoreboardPattern lobbyPattern) {
		this.lobbyPattern = lobbyPattern;
	}
	
	public ScoreboardPattern getGamePattern() {
		return gamePattern;
	}
	
	public void setGamePattern(ScoreboardPattern gamePattern) {
		this.gamePattern = gamePattern;
	}
	
	public GameRunnable getGameRunnable() {
		return gameRunnable;
	}
	
	public void setGameRunnable(GameRunnable gameRunnable) {
		this.gameRunnable = gameRunnable;
	}
	
	public int getAmountPlayers() {
		return amountPlayers;
	}
	
	public void setAmountPlayers(int amountPlayers) {
		this.amountPlayers = amountPlayers;
	}
	
	public TeamManager getTeamManager() {
		return teamManager;
	}

}
