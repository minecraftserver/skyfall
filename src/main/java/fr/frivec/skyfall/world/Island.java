package fr.frivec.skyfall.world;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.data.Directional;
import org.bukkit.inventory.ItemStack;

import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;

import fr.frivec.api.spigot.cuboid.Cuboid;
import fr.frivec.skyfall.SkyFall;

public class Island {
	
	private int layer;
	private Location center;
	
	private Cuboid cuboid;
	private Clipboard clipboard;
	
	private ArrayList<ItemStack> totalLoot;
	
	public Island(final int layer, final Location center, final Clipboard clipboard) {
		
		this.layer = layer;
		this.center = center;
		
		this.totalLoot = new ArrayList<>();
		
		setClipboard(clipboard);
		
	}
	
	public void destroy() {
				
		for(Block block : this.cuboid)
					
			if(!block.getType().equals(Material.AIR))
				
				block.setType(Material.AIR);
		
		this.center.getWorld().createExplosion(this.center, 5.0f);
		
	}
	
	/**
	 * Replace marks (gold blocks, iron_blocks, etc... by actual blocks)
	 */
	public void loadEmplacements() {
				
		List<Block> chestEmplacements = new ArrayList<>();
		final int numberOfchest = this.layer == 0 ? 3 : this.layer == -1 ? 4 : 2;
		
		if(this.cuboid == null)
			
			return;
		
		for(Block blocks : this.cuboid)
			
			if(blocks.getType().equals(Material.IRON_BLOCK) || blocks.getType().equals(Material.GOLD_BLOCK))
				
				chestEmplacements.add(blocks);
		
		if(chestEmplacements.isEmpty())
			
			return;
		
		final Random random = new Random();
		
		for(int i = 0; i < numberOfchest; i++) {
			
			final int index = random.nextInt(0, chestEmplacements.size() - (this.layer == -1 ? 0 : 1));						
			final Block block = chestEmplacements.get(index);
			
			block.setType(Material.CHEST);
			
			final Chest chest = (Chest) block.getState();
			final ArrayList<ItemStack> loots = SkyFall.getInstance().getLoots().getLoots(this, this.layer == 0 ? 0 : this.layer < 0 ? 2 : 1);
			
			/*
			 * Set up random facing for each chest
			 */
			
			final int chestFaceRandom = random.nextInt(1, 5);
			final Directional data = (Directional) chest.getBlockData();
			
			data.setFacing(chestFaceRandom == 1 ? BlockFace.EAST : chestFaceRandom == 2 ? BlockFace.WEST : chestFaceRandom == 3 ? BlockFace.NORTH : BlockFace.SOUTH);
			
			chest.setBlockData(data);
			chest.update();
			
			/*
			 * Adding all items to chest's inventory
			 */
			for(ItemStack item : loots)
			
				addItemRandomly(chest, item);
			
			chestEmplacements.remove(index);
				
		}
		
		for(Block emplacements : chestEmplacements)
			
			emplacements.setType(emplacements.getWorld().getBlockAt(emplacements.getLocation().clone().add(1, 0, 0)).getType());
		
		chestEmplacements.clear();
		
	}
	
	private void addItemRandomly(final Chest chest, final ItemStack item) {
		
		final Random random = new Random();
		final int slot = random.nextInt(0, chest.getInventory().getSize() - 1);
		
		if(chest.getInventory().getItem(slot) == null)
			
			chest.getInventory().setItem(slot, item);
		
		else
			
			addItemRandomly(chest, item);
		
	}

	public int getLayer() {
		return layer;
	}

	public void setLayer(int layer) {
		this.layer = layer;
	}

	public Location getCenter() {
		return center;
	}

	public void setCenter(Location center) {
		this.center = center;
	}
	
	public Clipboard getClipboard() {
		return clipboard;
	}
	
	public Cuboid getCuboid() {
		return cuboid;
	}
	
	public void setCuboid(Cuboid cuboid) {
		this.cuboid = cuboid;
	}
	
	public ArrayList<ItemStack> getTotalLoot() {
		return totalLoot;
	}
	
	public void setClipboard(Clipboard clipboard) {
		
		if(clipboard == null)
			
			return;
		
		this.clipboard = clipboard;
		
		final CuboidRegion region = clipboard.getRegion().getBoundingBox();
		final BlockVector3 origin = clipboard.getOrigin();
		
		this.cuboid = new Cuboid(this.center.clone().add(region.getPos1().getBlockX() - origin.getBlockX(), region.getPos1().getBlockY() - origin.getBlockY(), region.getPos1().getBlockZ() - origin.getBlockZ()),
				this.center.clone().add(region.getPos2().getBlockX() - origin.getBlockX(), region.getPos2().getBlockY() - origin.getBlockY(), region.getPos2().getBlockZ() - origin.getBlockZ()));
		
	}

}
