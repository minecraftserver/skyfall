package fr.frivec.skyfall.world;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import fr.frivec.api.spigot.title.Title;
import fr.frivec.skyfall.SkyFall;

public class WorldDestroyer extends BukkitRunnable {
	
	private WorldBuilder builder;
	
	private int layerToDestroy,
					islandIndex;
	
	private ArrayList<Island> islands;
	
	public WorldDestroyer(final WorldBuilder builder) {
		
		this.builder = builder;
		
	}
	
	@Override
	public void run() {
		
		if(this.islandIndex >= this.islands.size()) {
			
			Bukkit.getWorld("world").setTime(6000);
			
			this.cancel();
			return;
			
		}
		
		final Island island = this.islands.get(this.islandIndex);
		
		island.destroy();
		
		this.islandIndex++;
		
	}
	
	public void destroyLayer(final int layer) {
		
		this.layerToDestroy = layer;
		this.islands = this.builder.getIslands(layer, true);
		
		//Launch timer to destroy islands
		
		this.runTaskTimer(SkyFall.getInstance(), 6*10L, 10L);
		
		/*
		 * Set environment to night and show a title + play a sound
		 */
		
		Bukkit.getWorld("world").setTime(18000);
		Title.sendTitleToAllPlayers("§cApocalypse Time !", "", 1000, 2000, 1000);
		
		for(Player players : Bukkit.getOnlinePlayers())
			
			players.playSound(players, Sound.ENTITY_WITHER_SPAWN, 0.8f, 0.5f);
		
		//Player animation with fireballs
		
		for(int i = 0; i < this.islands.size(); i++) {
			
			final Island island = this.islands.get(i);
			
			new BukkitRunnable() {
				
				private int times = 3;
				
				@Override
				public void run() {
					
					if(times == 0) {
						
						this.cancel();
						return;
						
					}
					
					final Location location = island.getCenter(),
									fireballLocation = location.clone().add(new Vector(0, 25, 0));
					
					final double dX = fireballLocation.getX() - location.getX();
					final double dY = fireballLocation.getY() - location.getY();
					final double dZ = fireballLocation.getZ() - location.getZ();
					
					final Fireball fireball = (Fireball) location.getWorld().spawnEntity(fireballLocation, EntityType.FIREBALL);
					
					fireball.setIsIncendiary(false);
					fireball.setInvulnerable(true);
					fireball.setBounce(false);
					fireball.setDirection(new Vector(dX, dY, dZ).multiply(-1));
					
					times--;
					
				}
				
			}.runTaskTimer(SkyFall.getInstance(), i * 10L, 20L);
			
		}
		
	}
	
	public int getLayerToDestroy() {
		return layerToDestroy;
	}
	
	public void setLayerToDestroy(int layerToDestroy) {
		this.layerToDestroy = layerToDestroy;
	}
	
	public WorldBuilder getBuilder() {
		return builder;
	}

}
