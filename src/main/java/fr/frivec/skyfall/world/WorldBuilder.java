package fr.frivec.skyfall.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitRunnable;

import com.sk89q.worldedit.extent.clipboard.Clipboard;

import fr.frivec.api.core.logger.Logger.LogLevel;
import fr.frivec.api.core.servers.status.ServerStatus;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.cuboid.Cuboid;
import fr.frivec.skyfall.SkyFall;

public class WorldBuilder extends BukkitRunnable {
	
	private final World world;
	private final Random random;
	
	private final Location mapCenter;
	
	private HashMap<Integer, ArrayList<Island>> islands;
	
	private String[] previousThemes;
	private int distanceBetweenIslands;
	
	/*
	 * Location of island data
	 */
	
	private final int startDistance;
	private double angle, startAngle;
	private int layer;
	
	public WorldBuilder() {
		
		this.world = Bukkit.getWorld("world");
		this.mapCenter = new Location(this.world, 0, 0, 0);
		this.random = new Random();
		this.islands = new HashMap<>();
		this.startDistance = this.random.nextInt(100, 125);
		this.distanceBetweenIslands = this.random.nextInt(5, 10);
		this.previousThemes = new String[] {"", ""};
		this.angle = 0;
		this.startAngle = 0;
		this.layer = 0;
		
		for(int x = -10; x <= 10; x++)
			
			for(int z = -10; z <= 10; z++)
				
				this.world.getChunkAt(x, z).load();
		
	}
	
	@Override
	public void run() {
		
		final int distanceY = this.random.nextInt(-5, 0);
		
		if(this.angle > (Math.PI + this.startAngle)) {
			
			this.angle = this.random.nextDouble(0, Math.PI / (double) 4);
			this.distanceBetweenIslands = this.random.nextInt(5, 10);
			this.startAngle = this.angle;
			this.layer++;
			
		}
		
		final double pi = Math.PI / ((double) 6 + this.layer * 1.25);
		
		/*
		 * We have to do some calculations
		 * The islands have to be each to same distance of the center
		 * 
		 * The starts islands will be around 100 blocks away from the center
		 * For a better repartition of the islands, we will follow the trigonometric rules
		 */
		
		final double x = Math.cos(this.angle) * (this.startDistance - (this.layer * 20 - (this.layer == 0 ? this.distanceBetweenIslands : 0))), 
						z = Math.sin(this.angle) * (this.startDistance - (this.layer * 20 - (this.layer == 0 ? this.distanceBetweenIslands : 0)));
		
		/*
		 * If the distance between the center island and the new layer is under 20 blocks, we cancel the new layer
		 */
		if(Math.abs(x) <= 25 && Math.abs(z) <= 25) {
			
			this.cancel();
			
			SpigotAPI.log(LogLevel.INFO, "§3End of generation at layer §a" + this.layer + "§3. Starting chest loading...");
			
			this.islands.values().forEach(list -> {
				
				for(Island island : list)
					
					//Load the chests and players spawns
					island.loadEmplacements();
				
			});
			
			SpigotAPI.log(LogLevel.INFO, "§3End of chests loading. §aServer is now accessible");
			
			//Spawn lobby on the map
			final Island lobby = new Island(-2, new Location(Bukkit.getWorld("world"), 0, 60, 0), SkyFall.getInstance().getSchemLoader().loadSchematic("SF_Lobby"));
			final ArrayList<Island> islands = new ArrayList<>();
			
			islands.add(lobby);
			
			this.islands.put(-2, islands);
			
			SkyFall.getInstance().getSchemLoader().pasteSchematic(lobby, lobby.getClipboard());
			
			SpigotAPI.getInstance().updateServerStatus(ServerStatus.OPEN);
			
			return;
			
		}
		
		final Location firstIslandCenter = new Location(this.world, x, this.mapCenter.getBlockY() - distanceY, z),
						secondIslandCenter = firstIslandCenter.clone().set(-x, this.mapCenter.getBlockY() - this.random.nextInt(-5, 0), -z);
		
		addIsland(0, firstIslandCenter);
		addIsland(1, secondIslandCenter);
		
		this.angle += pi;
		
	}
	
	/**
	 * Add an island on the map
	 * @param index: Index of the island for this generation (if it's the first or second island)
	 * @param location: Location of the paste origin
	 */
	private void addIsland(int index, final Location location) {
		
		Island island = null;
		
		if((island = spawnIslands(index, location)) != null)
		
			this.getIslands(this.layer, false).add(island);
		
	}
	
	/**
	 * Spawn an island on the map by using a WorldEdit schematic
	 * @param index: Index of the island
	 * @param location: Location of the paste origin
	 * @return a new Island object with the informations of the new island
	 */
	private Island spawnIslands(int index, final Location location) {
		
		final String schematicName = getRandomSchematic(index);
		final Clipboard schematic = SkyFall.getInstance().getSchemLoader().loadSchematic(schematicName);
		
		Island island = new Island(this.layer, location, schematic);
		
		if(!SkyFall.getInstance().getSchemLoader().pasteSchematic(island, schematic))
			
			island = null;
		
		else
			
			this.previousThemes[index] = schematicName.split("[_]")[0];
		
		return island;
		
	}
	
	/**
	 * Get a random schematic name from all the names registered when the plugin is starting.
	 * @param index: Index of the island
	 * @return a schematic name with a different theme than the previous island
	 */
	private String getRandomSchematic(int index) {
		
		final List<String> schematicsName = SkyFall.getInstance().getSchematics();
		final Random random = new Random();
		
		final int id = random.nextInt(0, schematicsName.size() - 1);
		final String schematicName = schematicsName.get(id);
		final String theme = schematicName.split("[_]")[0];
		
		if(this.previousThemes[index] != null && theme.equalsIgnoreCase(this.previousThemes[index]))
			
			return getRandomSchematic(index);
		
		else
			
			return schematicName;
		
	}
	
	public World getWorld() {
		return world;
	}
	
	public Island getIslandByCenter(final int layer, final Location location) {
			
		for(Island island : this.islands.get(layer))
				
			if(island.getCenter().equals(location))
					
				return island;
		
		return null;
		
	}
	
	public Island getIslandByCenter(final Location location) {
		
		Island island = null;
		
		for(int layers : this.islands.keySet())
			
			island = getIslandByCenter(layers, location);
		
		return island;
		
	}
	
	public ArrayList<Island> getIslands(final int lineIndex, final boolean destroy) {
		
		if(this.islands.containsKey(lineIndex))
			
			return this.islands.get(lineIndex);
		
		else {
			
			if(destroy)
				
				return null;
			
			else {
			
				this.islands.put(lineIndex, new ArrayList<>());
				
				return getIslands(lineIndex, destroy);
				
			}
		
		}
			
	}
	
	public void registerCenterIsland(final Location center, final Cuboid cuboid) {
		
		final Island centerIsland = new Island(-1, center, null);
		final ArrayList<Island> list = new ArrayList<>();
		
		centerIsland.setCuboid(cuboid);
		
		list.add(centerIsland);
		
		this.islands.put(-1, list);
		
	}
	
	public int getMaxLayer() {
		
		return this.islands.size() - 2; //- 2 because of the two exception islands (center and lobby)
		
	}

}
