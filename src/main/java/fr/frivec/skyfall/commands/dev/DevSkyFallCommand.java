package fr.frivec.skyfall.commands.dev;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_18_R2.CraftWorld;
import org.bukkit.entity.Player;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.commands.AbstractCommand;
import fr.frivec.api.spigot.packets.PacketUtils;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.kits.KitList;
import fr.frivec.skyfall.kits.model.Kit;
import fr.frivec.skyfall.runnables.LobbyRunnable;
import fr.frivec.skyfall.utils.States;
import fr.frivec.skyfall.world.WorldBuilder;
import fr.frivec.skyfall.world.WorldDestroyer;
import net.minecraft.network.protocol.game.PacketPlayOutCamera;
import net.minecraft.world.level.World;

public class DevSkyFallCommand extends AbstractCommand {
	
	private WorldBuilder builder;
	
	public DevSkyFallCommand() {
		
		super("devsf", "/devsf <args>", "The dev command for SkyFall plugin", "§cErreur. Vous n'avez pas la permission d'utiliser cette commande.", Arrays.asList("devskyfall"));
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			final Player player = (Player) sender;
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
			
			if(localPlayer.getRank().getPower() >= RankList.DEV.getPower()) {
				
				if(args[0].equalsIgnoreCase("testTrigo")) {
					
					for(double angle = 0; angle <= 2*Math.PI; angle += Math.PI / (double) 6) {
					
					final double distance = 10,
									x = Math.cos(angle) * distance,
									z = Math.sin(angle) * distance;
						
						player.getWorld().getBlockAt((int) Math.round(x), player.getLocation().getBlockY(), (int) Math.round(z)).setType(Material.DIAMOND_BLOCK);					
					
					}
					
				}else if(args[0].equalsIgnoreCase("testspawn")) {
					
					this.builder = new WorldBuilder();
					
					builder.runTaskTimer(SkyFall.getInstance(), 0L, 20L);
					
				}else if(args[0].equalsIgnoreCase("destroyLayer")) {
					
					if(args.length > 1) {
						
						final WorldDestroyer destroyer = new WorldDestroyer(this.builder);
						
						destroyer.destroyLayer(Integer.parseInt(args[1]));
						
					}else
						
						return false;
					
				}else if(args[0].equalsIgnoreCase("states")) {
					
					if(args.length <= 1)
						
						player.sendMessage(text("Current state: " + SkyFall.getInstance().getCurrentState().toString()));
					
					else {
						
						final String stateName = args[1];
						States state = null;
						
						for(States states : States.values()) {
							
							if(states.toString().equalsIgnoreCase(stateName)) {
								
								state = states;
								break;
								
							}
							
						}
						
						if(state != null) {
							
							SkyFall.getInstance().setStates(state);
							player.sendMessage(text("§aState set to " + state.toString()));
							
						}else
							
							player.sendMessage(text("§cNo state found for name: " + stateName));
						
					}
					
					return true;
					
				}else if(args[0].equalsIgnoreCase("testkit")) {
					
					final Kit kit = KitList.TEST.getKit();
					
					kit.giveKit(player);
					
				}else if(args[0].equalsIgnoreCase("forcestart")) {
					
					LobbyRunnable.start();
					player.sendMessage(text("§aLobby runnable lancé"));
					
				}else if(args[0].equalsIgnoreCase("camera")) {
					
					if(args[1] != null && !args[1].equalsIgnoreCase("")) {
						
						final Player camera = Bukkit.getPlayer(args[1]);
						
						if(camera != null)
							
							setPlayerCamera(player, camera);
						
					}
					
				}
				
			}else
				
				sender.sendMessage(text(this.permMessage));
			
		}
		
		return false;
	}
	
	private void setPlayerCamera(final Player player, final Player newCamera) {
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(SkyFall.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				
				System.out.println("test 1");
				
				final World world = ((CraftWorld) player.getWorld()).getHandle();
				
				final PacketPlayOutCamera packet = new PacketPlayOutCamera(world.a(newCamera.getEntityId()));
				
				PacketUtils.sendPacket(player, packet);
				
			}
			
		}, 10L);
		
	}

}
