package fr.frivec.skyfall.commands.admin;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.commands.AbstractCommand;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.runnables.LobbyRunnable;
import fr.frivec.skyfall.utils.States;
import fr.frivec.skyfall.utils.Utils;

public class StartCommand extends AbstractCommand {
	
	public StartCommand() {
		
		super("start", "/start", "A command to start the game as an admin.", "§cErreur. Vous n'avez pas la permission d'utiliser cette commande.", Arrays.asList(""));

	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player) {
			
			final Player player = (Player) sender;
			final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
			
			if(localPlayer.getRank().getPower() >= RankList.ADMIN.getPower()) {
				
				if(!SkyFall.getInstance().getCurrentState().equals(States.WAIT)) {
					
					player.sendMessage(text("§cLa partie est en cours."));
					return true;
					
				}
				
				final LobbyRunnable lobbyRunnable = SkyFall.getInstance().getLobbyRunnable();
				
				if(lobbyRunnable != null && lobbyRunnable.isStart()) {
					
					player.sendMessage(text("§cLa partie a déjà commencé."));
					
					return true;
					
				}
				
				LobbyRunnable.start();
				
				SkyFall.getInstance().getLobbyRunnable().setAdminStart(true);
				SkyFall.getInstance().getLobbyRunnable().setTimer(30);
				SkyFall.getInstance().getLobbyRunnable().setStartTimer(30);
				
				Bukkit.getServer().sendMessage(text(Utils.PREFIX + " §aDémarrage de la partie forcé par " + localPlayer.getRank().getPrefix() + " " + player.getName() + "§a."));
				
				return true;
				
			}else {
				
				player.sendMessage(text(this.permMessage));
				return true;
				
			}
			
		}else {
			
			sender.sendMessage(text("§cErreur. Veuillez utiliser un compte joueur pour utiliser cette commande."));
			return true;
			
		}
		
	}

}
