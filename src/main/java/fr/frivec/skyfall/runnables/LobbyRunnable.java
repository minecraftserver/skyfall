package fr.frivec.skyfall.runnables;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import fr.frivec.api.spigot.effects.gloweffect.GlowEffect.EffectColor;
import fr.frivec.api.spigot.teams.Team;
import fr.frivec.api.spigot.teams.client.NMSTeam;
import fr.frivec.api.spigot.title.Title;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.kits.KitList;
import fr.frivec.skyfall.player.SkyfallPlayer;
import fr.frivec.skyfall.teams.Teams;
import fr.frivec.skyfall.utils.States;
import fr.frivec.skyfall.utils.Utils;
import fr.frivec.skyfall.world.Island;

public class LobbyRunnable extends BukkitRunnable {
	
	private double timer;
	private double startTimer;
	
	private boolean adminStart,
					voteStart,
					start;
	
	private HashSet<Integer> takenIslands;
	
	public LobbyRunnable() {
		
		this.timer = this.startTimer = 120.0d;
		
		this.adminStart = this.voteStart = this.start = false;
		
	}
	
	@Override
	public void run() {
		
		if(this.timer == 120 || this.timer == 60 || this.timer == 30 || this.timer == 10 || this.timer == 5 || this.timer == 4 || this.timer == 3 || this.timer == 2 || this.timer == 1)
		
			Title.sendTitleToAllPlayers("§6Démarrage", "§bDémarrage dans §e" + this.timer + " §bseconde" + (this.timer == 1 ? "" : "s"), 1000, 2000, 1000);
			
		if(this.timer == 0) {
			
			Bukkit.getServer().sendMessage(text(Utils.PREFIX + " §aLa partie démarre. Téléportation..."));
			
			final Random random = new Random();
			this.takenIslands = new HashSet<>();
			
			int i = 1;
			
			for(SkyfallPlayer skyfallPlayer : SkyFall.getInstance().getPlayers()) {
				
				final Player player = skyfallPlayer.getPlayer();
				
				//Set player game mode
				player.setGameMode(GameMode.SURVIVAL);
				
				//Clear inventory
				player.getInventory().clear();
				
				//Teleport player
				teleportPlayer(random, player);
				
				//Give kit
				if(skyfallPlayer.getKit() == null) {
					
					player.sendMessage(text("§cVous n'avez pas sélectionné de kit. Vous obtenez alors le kit par défaut: null"));
					skyfallPlayer.setKit(KitList.TEST.getKit());
					
				}
				
				skyfallPlayer.getKit().giveKit(player);
				
				/*
				 * Teams
				 */
				
				//Add player to team
				final Team team = Teams.getById(i).getTeam();
				final NMSTeam nmsTeam = team.getNmsTeam();
				
				for(SkyfallPlayer allPlayers : SkyFall.getInstance().getPlayers())
						
					//Add player in team for all players
					nmsTeam.addPlayer(player, Bukkit.getPlayer(allPlayers.getUuid()));
				
				//Change the prefix for the player
				nmsTeam.setPrefix("§a✔ ");
				nmsTeam.setColor(EffectColor.LIME);
				nmsTeam.updateTeam(player); //Update team packet
				
				i++;
				
			}
			
			//Set number of player in scoreboard
			SkyFall.getInstance().getGamePattern().setLine(6, "§6Joueur(s) en vie ➭ §f" + SkyFall.getInstance().getAmountPlayers()); 
			
			//Set new game state
			SkyFall.getInstance().setStates(States.GAME);
			
			//Destroy lobby
			final Island lobby = SkyFall.getInstance().getWorldBuilder().getIslands(-2, false).get(0);
			
			for(Block blocks : lobby.getCuboid())
				
				if(!blocks.getType().equals(Material.AIR))
			
					blocks.setType(Material.AIR);
			
			//Delete all items falling due to lobby destruction
			final List<Entity> entities = Bukkit.getWorld("world").getEntities();
			
			entities.iterator().forEachRemaining(entity -> {
				
				if(entity.getType().equals(EntityType.DROPPED_ITEM))
					
					entity.remove();
				
			});
					
			//Start game runnable
			this.stop(false);
			SkyFall.getInstance().getGameRunnable().start();
			
		}
		
		this.getBossbar().setProgress((this.startTimer - this.timer) / this.startTimer);
		this.getBossbar().setTitle("§bDémarrage dans §e" + this.timer + " §bseconde" + (this.timer == 1 ? "" : "s"));
		
		this.timer--;
		
	}
	
	public void teleportPlayer(final Random random, final Player player) {
		
		final int id = random.nextInt(0, 11);
		
		if(!this.takenIslands.contains(id)) {
			
			final Island island = SkyFall.getInstance().getWorldBuilder().getIslands(0, false).get(id);
			
			player.teleport(island.getCenter().add(0, 0.5, 0));
					
			this.takenIslands.add(id);
			
			return;
			
		}else
			
			teleportPlayer(random, player);
		
	}
	
	public void stop(final boolean lackPlayer) {
		
		this.cancel();
		this.start = false;
		
		if(lackPlayer) {
		
			this.getBossbar().setProgress(1.0);
			this.getBossbar().setColor(BarColor.RED);
			this.getBossbar().setTitle("§bDémarrage annulé - Manque de joueurs");
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(SkyFall.getInstance(), new Runnable() {
				
				@Override
				public void run() {
					
					getBossbar().setColor(BarColor.YELLOW);
					getBossbar().setTitle("§7En attente de joueurs...");
					
				}
				
			}, 5*20L);
			
		}else
			
			this.getBossbar().removeAll();
		
	}
	
	public static void checkVoteStart() {
		
		LobbyRunnable lobbyRunnable = SkyFall.getInstance().getLobbyRunnable();
		final HashSet<SkyfallPlayer> players = SkyFall.getInstance().getPlayers();
		
		if(players.size() > 1 && players.size() < 6) {
			
			for(SkyfallPlayer player : players) {
				
				if(!player.isVoteStart()) { //If one player didn't vote for the start, cancel the start
					
					if(lobbyRunnable != null && lobbyRunnable.voteStart && lobbyRunnable.start && !lobbyRunnable.adminStart) {
						
						lobbyRunnable.voteStart = false;
						lobbyRunnable.stop(true);
						
					}
					
					return;
					
				}
				
			}
			
			start();
			
			lobbyRunnable = SkyFall.getInstance().getLobbyRunnable();
			lobbyRunnable.voteStart = true;
			
		}
		
		return;
		
	}
	
	public static void start() {
		
		final LobbyRunnable lobbyRunnable = new LobbyRunnable();
		
		SkyFall.getInstance().setLobbyRunnable(lobbyRunnable);
		
		lobbyRunnable.getBossbar().setColor(BarColor.GREEN);
		lobbyRunnable.start = true;
		
		lobbyRunnable.runTaskTimer(SkyFall.getInstance(), 0L, 20L);
		
	}
	
	public void setTimer(double timer) {
		this.timer = timer;
	}
	
	public double getTimer() {
		return timer;
	}
	
	public BossBar getBossbar() {
		return SkyFall.getInstance().getBossBar();
	}
	
	public boolean isAdminStart() {
		return adminStart;
	}
	
	public void setAdminStart(boolean adminStart) {
		this.adminStart = adminStart;
	}
	
	public double getStartTimer() {
		return startTimer;
	}
	
	public void setStartTimer(double startTimer) {
		this.startTimer = startTimer;
	}
	
	public boolean isVoteStart() {
		return voteStart;
	}
	
	public void setVoteStart(boolean voteStart) {
		this.voteStart = voteStart;
	}
	
	public boolean isStart() {
		return start;
	}
	
	public void setStart(boolean start) {
		this.start = start;
	}

}
