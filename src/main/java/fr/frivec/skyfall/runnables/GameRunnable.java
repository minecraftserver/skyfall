package fr.frivec.skyfall.runnables;

import java.text.SimpleDateFormat;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import fr.frivec.api.spigot.scoreboard.ScoreboardPattern;
import fr.frivec.api.spigot.scoreboard.client.NMSScoreboard;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.player.SkyfallPlayer;
import fr.frivec.skyfall.world.WorldDestroyer;

public class GameRunnable extends BukkitRunnable {
	
	private boolean start;
	
	private int timer,
				apocalypseTimer,
				layerToDestroy;
	
	private boolean isDestructionEnded;
	
	private Random random;
	
	private SkyFall skyFall;
	
	public GameRunnable() {
		
		this.start = false;
		this.isDestructionEnded = false;
		
		this.skyFall = SkyFall.getInstance();
		this.random = new Random();
		
		this.timer = 0;
		this.apocalypseTimer = this.random.nextInt(60, 90);
		
	}
	
	@Override
	public void run() {
		
		if(this.apocalypseTimer == 0 && !this.isDestructionEnded) {
			
			final WorldDestroyer destroyer = new WorldDestroyer(this.skyFall.getWorldBuilder());
			
			destroyer.destroyLayer(this.layerToDestroy);
			
			this.apocalypseTimer = this.random.nextInt(60, 120);
			this.layerToDestroy++;
			
			if(this.layerToDestroy >= this.skyFall.getWorldBuilder().getMaxLayer())
				
				this.isDestructionEnded = true;
			
		}
		
		final ScoreboardPattern gamePattern = skyFall.getGamePattern();
		
		gamePattern.setLine(12, "§4Durée ➭ §f" + new SimpleDateFormat("mm:ss").format(getTimer() * 1000));
		
		for(Player players : Bukkit.getOnlinePlayers()) {
			
			final NMSScoreboard scoreboard = NMSScoreboard.getScoreboard(players);
			
			gamePattern.setLine(8, "§aTué(s) ➭  §f" + SkyfallPlayer.getPlayer(players.getUniqueId()).getGameStats().getKills());
			
			scoreboard.setPattern(gamePattern);
			scoreboard.refreshScoreboard();
				
		}
		
		this.timer++;
		
		if(!this.isDestructionEnded)
		
			this.apocalypseTimer--;
		
	}
	
	public void start() {
		
		this.start = true;
		this.runTaskTimer(SkyFall.getInstance(), 0L, 20L);
		
	}
	
	public void stop() {
		
		this.start = false;
		this.cancel();
		
	}
	
	public int getTimer() {
		return timer;
	}
	
	public void setTimer(int timer) {
		this.timer = timer;
	}
	
	public boolean isStart() {
		return start;
	}
	
	public void setStart(boolean start) {
		this.start = start;
	}

}
