package fr.frivec.skyfall.kits;

import org.bukkit.Color;
import org.bukkit.Material;

import fr.frivec.api.spigot.item.ItemCreator;
import fr.frivec.skyfall.kits.model.Kit;

public class TestKit extends Kit {
	
	public TestKit() {
		
		super("Test", Material.BARRIER, "§aLorem Ipsum", "", "");
		
	}
	
	@Override
	public void registerItems() {
		
		addItem(0, new ItemCreator(Material.NETHERITE_SWORD, 1).setDisplayName("§3Lorem Ipsum").build());
		addItem(-1, new ItemCreator(Material.BAKED_POTATO, 1).leatherArmor(Material.LEATHER_HELMET, Color.RED).build());
		addItem(-1, new ItemCreator(Material.BAKED_POTATO, 1).leatherArmor(Material.LEATHER_CHESTPLATE, Color.WHITE).build());
		addItem(-1, new ItemCreator(Material.BAKED_POTATO, 1).leatherArmor(Material.LEATHER_LEGGINGS, Color.WHITE).build());
		addItem(-1, new ItemCreator(Material.BAKED_POTATO, 1).leatherArmor(Material.LEATHER_BOOTS, Color.RED).build());
		
	}

}
