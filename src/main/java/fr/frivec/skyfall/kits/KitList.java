package fr.frivec.skyfall.kits;

import fr.frivec.skyfall.kits.model.Kit;

public enum KitList {
	
	TEST(0, new TestKit());
	
	private int id;
	private Kit kit;
	
	private KitList(final int id, final Kit kit) {
		
		this.id = id;
		this.kit = kit;
		
	}
	
	public static Kit getKitById(final int id) {
		
		for(KitList list : KitList.values())
			
			if(list.getId() == id)
				
				return list.getKit();
		
		return null;
		
	}
	
	public Kit getKit() {
		return kit;
	}
	
	public int getId() {
		return id;
	}

}
