package fr.frivec.skyfall.kits.item;

import org.bukkit.inventory.ItemStack;

public class KitItem {
	
	private int slot;
	private ItemStack item;
	
	public KitItem(final int slot, final ItemStack item) {
		
		this.slot = slot;
		this.item = item;
		
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public ItemStack getItem() {
		return item;
	}

	public void setItem(ItemStack item) {
		this.item = item;
	}

}
