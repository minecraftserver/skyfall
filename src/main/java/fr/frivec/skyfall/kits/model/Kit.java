package fr.frivec.skyfall.kits.model;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import fr.frivec.skyfall.kits.item.KitItem;

public abstract class Kit {
	
	protected String name;
	protected String[] description;
	protected Material icon;
	
	protected ArrayList<KitItem> items;

	public Kit(String name, Material icon, ArrayList<KitItem> items, String... description) {
		
		this.name = name;
		this.description = description;
		this.icon = icon;
		this.items = items;
		
		registerItems();
	
	}
	
	public Kit(String name, Material icon, String... description) {
		
		this(name, icon, new ArrayList<>(), description);
	
	}
	
	public abstract void registerItems();
	
	public void giveKit(final Player player) {
		
		for(KitItem kitItem : this.items) {
			
			final ItemStack item = kitItem.getItem();
			final String type = item.getType().toString();
			
			if(type.contains("HELMET"))
				
				player.getInventory().setHelmet(item);
			
			else if(type.contains("CHESTPLATE"))
				
				player.getInventory().setChestplate(item);
			
			else if(type.contains("LEGGINGS"))
				
				player.getInventory().setLeggings(item);
			
			else if(type.contains("BOOTS"))
				
				player.getInventory().setBoots(item);
			
			else
				
				player.getInventory().setItem(0, item);
			
		}
		
	}
	
	public void addItem(final int slot, final ItemStack item) {
		
		this.items.add(new KitItem(slot, item));
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getDescription() {
		return description;
	}

	public void setDescription(String[] description) {
		this.description = description;
	}

	public Material getIcon() {
		return icon;
	}

	public void setIcon(Material icon) {
		this.icon = icon;
	}

	public ArrayList<KitItem> getItems() {
		return items;
	}

	public void setItems(ArrayList<KitItem> items) {
		this.items = items;
	}
	
}
