package fr.frivec.skyfall.player.statistics;

public class Statistics {
	
	private int kills,
				assits,
				deaths;
	
	private long timePlayed;

	public Statistics(int kills, int assits, int deaths, long timePlayed) {
		
		this.kills = kills;
		this.assits = assits;
		this.deaths = deaths;
		this.timePlayed = timePlayed;
	
	}
	
	public void increaseKills() {
		
		this.kills++;
		
	}
	
	public void increaseDeaths() {
		
		this.deaths++;
		
	}
	
	public void increaseAssits() {
		
		this.assits++;
		
	}

	public int getKills() {
		return kills;
	}

	public void setKills(int kills) {
		this.kills = kills;
	}

	public int getAssits() {
		return assits;
	}

	public void setAssits(int assits) {
		this.assits = assits;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public long getTimePlayed() {
		return timePlayed;
	}

	public void setTimePlayed(long timePlayed) {
		this.timePlayed = timePlayed;
	}

}
