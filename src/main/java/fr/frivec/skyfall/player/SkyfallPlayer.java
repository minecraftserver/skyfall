package fr.frivec.skyfall.player;

import java.util.HashSet;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.spigot.teams.Team;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.kits.model.Kit;
import fr.frivec.skyfall.player.statistics.Statistics;

public class SkyfallPlayer {
	
	private UUID uuid;
	private Team team;
	private Kit kit;
	
	//The UUID of players that damage this player
	private HashSet<UUID> assists;
	
	private Statistics gameStats, allTimeStats;
	private boolean voteStart, spectating;
	
	//Spectating
	private UUID playerSpectating; //Spectating id (id of the player in the list)
	
	private transient Player player;
	private transient LocalPlayer localPlayer;
	
	public SkyfallPlayer(final Player player) {
		
		this.uuid = player.getUniqueId();
		this.player = player;
		this.localPlayer = LocalPlayer.getLocalPlayer(this.uuid);
		this.assists = new HashSet<>();
		
		this.spectating = false;
		this.playerSpectating = null;
		this.team = null;
		
		this.gameStats = new Statistics(0, 0, 0, 0);
		this.voteStart = false;
		this.kit = null;
		
	}
	
	public SkyfallPlayer(final UUID uuid) {
		
		this(Bukkit.getPlayer(uuid));
		
	}
	
	public static SkyfallPlayer getPlayer(final UUID uuid) {
		
		for(SkyfallPlayer player : SkyFall.getInstance().getPlayers())
			
			if(player.getUuid().equals(uuid))
				
				return player;
		
		return null;
		
	}
	
	public HashSet<UUID> getAssists() {
		return assists;
	}
	
	public void setAssists(HashSet<UUID> assists) {
		this.assists = assists;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}
	
	public Kit getKit() {
		return kit;
	}
	
	public void setKit(Kit kit) {
		this.kit = kit;
	}

	public Statistics getGameStats() {
		return gameStats;
	}

	public void setGameStats(Statistics gameStats) {
		this.gameStats = gameStats;
	}

	public Statistics getAllTimeStats() {
		return allTimeStats;
	}

	public void setAllTimeStats(Statistics allTimeStats) {
		this.allTimeStats = allTimeStats;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public LocalPlayer getLocalPlayer() {
		return localPlayer;
	}
	
	public boolean isVoteStart() {
		return voteStart;
	}
	
	public void setVoteStart(boolean voteStart) {
		this.voteStart = voteStart;
	}
	
	public boolean isSpectating() {
		return spectating;
	}
	
	public void setSpectating(boolean spectating) {
		this.spectating = spectating;
	}
	
	public UUID getPlayerSpectating() {
		return playerSpectating;
	}
	
	public void setPlayerSpectating(UUID playerSpectating) {
		this.playerSpectating = playerSpectating;
	}
	
}
