package fr.frivec.skyfall.managers;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.item.ItemCreator;
import fr.frivec.api.spigot.managers.AbstractManager;
import fr.frivec.api.spigot.scoreboard.client.NMSScoreboard;
import fr.frivec.api.spigot.teams.Team;
import fr.frivec.api.spigot.title.ActionBar;
import fr.frivec.api.spigot.title.Title;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.player.SkyfallPlayer;
import fr.frivec.skyfall.runnables.LobbyRunnable;
import fr.frivec.skyfall.teams.Teams;
import fr.frivec.skyfall.utils.States;

public class PlayerManager extends AbstractManager {
	
	public PlayerManager() {
		
		super(SkyFall.getInstance());
		
	}
	
	@Override
	public void onConnect(Player player) {
		
		final SkyFall skyFall = SkyFall.getInstance();
		final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
		final States currentState = skyFall.getCurrentState();
		
		//Add player to player(s list
		skyFall.getPlayers().add(new SkyfallPlayer(player));
		
		/*
		 * Create teams for the player (NMSTeam)
		 */
		for(Teams teams : Teams.values())
			
			teams.getTeam().getNmsTeam().createTeam(player);
		
		/*
		 * Set the connect listener according to the current state of the game
		 */
		switch (currentState) {
		
		case WAIT: {
			
			final LobbyRunnable lobbyRunnable = skyFall.getLobbyRunnable();
			
			skyFall.setAmountPlayers(skyFall.getAmountPlayers() + 1);
			
			player.setGameMode(GameMode.ADVENTURE);
			player.getInventory().clear();
			
			ActionBar.sendActionBarToAllPlayer(text(localPlayer.getRank().getPrefix() + (localPlayer.getRank().equals(RankList.PLAYER) ? "§7" : " ") + player.getName() 
													+ " §aa rejoint la partie §7(" + skyFall.getPlayers().size() + "/12)"));
			
			Title.sendTitleToPlayer(player, "§6SkyFall", "", 1000, 2000, 1000);
			
			/*
			 * Gives menus' items
			 */
			
			final ItemStack kitsSelector = new ItemCreator(Material.NETHER_STAR, 1).setDisplayName("§6Sélectionner un kit").setLores("§b§oUtilisez un clique-droit", "§b§opour ouvrir le menu de sélection des kits.").addTag("LOBBY_ITEM", "KITS").build(),
							hubItem = new ItemCreator(Material.RED_BED, 1).setDisplayName("§cRetour au hub").addTag("LOBBY_ITEM", "HUB").build(),
							startItem = new ItemCreator(Material.SLIME_BALL, 1).setDisplayName("§aCommencer la partie").setLores("§7Vous votez pour lancer la partie", "§7sans qu'elle n'est suffisamment", "§7de joueurs pour", "§7démarrer automatiquement.", "", "§3§oNécessite que tous les joueurs", "§3§odprésents votent pour le démarrage.").addTag("LOBBY_ITEM", "VOTE").build();
//							teamSelector = new ItemCreator(Material.NAME_TAG, 1).setDisplayName("§6Sélectionner une équipe").setLores("§b§oSélectionner une équipe").build();
			
			player.getInventory().setItem(0, startItem);
			player.getInventory().setItem(4, kitsSelector);
			player.getInventory().setItem(8, hubItem);
			
			/*
			 * Add player to bossbar players' list
			 */
			skyFall.getBossBar().addPlayer(player);
			
			/*
			 * Teleport player to lobby
			 */
			player.teleport(new Location(Bukkit.getWorld("world"), 0.5, 60.5, 0.5, -90.0f, 0.0f));
			
			/*
			 * Create scoreboard
			 */
			
			if(skyFall.getGamePattern().getLines()[2].contains("null"))
				
				skyFall.getGamePattern().setLine(2, "§9Serveur ➭ §f" + SkyFall.getApi().getInformations().getName());
			
			final NMSScoreboard scoreboard = new NMSScoreboard(player, skyFall.getLobbyPattern());
			
			scoreboard.create();
			
			skyFall.getLobbyPattern().setLine(5, "§6Joueurs en jeu: §f" + skyFall.getAmountPlayers());
			
			for(SkyfallPlayer players : skyFall.getPlayers()) {
				
				final NMSScoreboard playerScoreboard = NMSScoreboard.getScoreboard(players.getPlayer());
				
				if(!players.getUuid().equals(player.getUniqueId()))
					
					playerScoreboard.setPattern(skyFall.getLobbyPattern());
				
				playerScoreboard.refreshScoreboard();
				
			}
			
			/*
			 * Start Runnable if enough players 
			 */
			if(skyFall.getPlayers().size() >= 6 && (lobbyRunnable == null || !lobbyRunnable.isStart()))
				
				LobbyRunnable.start();
			
			else if(skyFall.getPlayers().size() >= 10 && lobbyRunnable != null && lobbyRunnable.isStart() && lobbyRunnable.getTimer() < 30)
				
				lobbyRunnable.setTimer(30);
			
			else if(skyFall.getPlayers().size() == 12 && lobbyRunnable != null && lobbyRunnable.isStart() && lobbyRunnable.getTimer() < 10)
				
				lobbyRunnable.setTimer(10);
			
			break;
			
		}
		
		case GAME: {
			
			final Team team = Teams.SPECTATOR.getTeam(); //Get spectator team
			
			//TODO Finish team
			
		}
		
		case END: {
			
			
			
		}
		
		default:
			throw new IllegalArgumentException("Unexpected value: " + currentState);
		}
		
	}
	
	@Override
	public void onDisable() {}
	
	@Override
	public void onDisconnect(Player player) {
		
		final SkyFall skyFall = SkyFall.getInstance();
		
		final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
		final LocalPlayer localPlayer = skyfallPlayer.getLocalPlayer();
		final States currentState = skyFall.getCurrentState();
		
		/*
		 * Delete teams for the player (NMSTeam)
		 */
		for(Team teams : skyFall.getTeamManager().getTeams())
			
			teams.getNmsTeam().deleteTeam(player);
		
		switch (currentState) {
		
		case WAIT: {
			
			final LobbyRunnable lobbyRunnable = skyFall.getLobbyRunnable();
			
			if(skyFall.getPlayers().contains(skyfallPlayer) && !skyfallPlayer.isSpectating()) {
				
				skyFall.setAmountPlayers(skyFall.getAmountPlayers() - 1);
			
				ActionBar.sendActionBarToAllPlayer(text(localPlayer.getRank().getPrefix() + (localPlayer.getRank().equals(RankList.PLAYER) ? "§7" : " ") + player.getName() 
				+ " §ca quitté la partie §7(" + skyFall.getPlayers().size() + "/12)"));
				
				skyFall.getLobbyPattern().setLine(5, "§6Joueurs en jeu: §f" + skyFall.getAmountPlayers());
			
				for(SkyfallPlayer players : skyFall.getPlayers()) {
						
					final NMSScoreboard scoreboard = NMSScoreboard.getScoreboard(players.getPlayer());
						
					scoreboard.setPattern(skyFall.getLobbyPattern());
					scoreboard.refreshScoreboard();
					
				}
				
			}
			
			skyFall.getBossBar().removePlayer(player);
			
			/*
			 * Stop runnable if not enough player
			 */
			
			if(lobbyRunnable != null)
			
				if(lobbyRunnable.isStart() && !lobbyRunnable.isAdminStart() && skyFall.getPlayers().size() < 6)
					
					lobbyRunnable.stop(true);
				
				else if(lobbyRunnable.isStart() && lobbyRunnable.isAdminStart() && skyFall.getPlayers().size() == 0)
					
					lobbyRunnable.stop(true);
			
			break;
			
		}
		
		case GAME: {
			
			if(skyfallPlayer != null) {
				
				if(!skyfallPlayer.isSpectating()) {
					
					final List<ItemStack> drops = new ArrayList<>();
					
					for(ItemStack items : player.getInventory().getContents())
						
						drops.add(items);
					
					Bukkit.getServer().getPluginManager().callEvent(new PlayerDeathEvent(player, drops, 0, ""));
					
				}
				
				SkyFall.getInstance().getPlayers().remove(skyfallPlayer);
				
				Bukkit.getServer().sendMessage(text(localPlayer.getRank().getPrefix() + (localPlayer.getRank().equals(RankList.PLAYER) ? "§7" : " ") + player.getName() 
				+ " §ca quitté la partie §7(" + skyFall.getPlayers().size() + "/12)"));
				
			}
			
		}
		
		case END: {
			
			Bukkit.getServer().sendMessage(text(localPlayer.getRank().getPrefix() + (localPlayer.getRank().equals(RankList.PLAYER) ? "§7" : " ") + player.getName() 
			+ " §ca quitté la partie §7(" + skyFall.getPlayers().size() + "/12)"));
			
		}
		
		default:
			throw new IllegalArgumentException("Unexpected value: " + currentState);
		}
		
		//Remove player from list
		skyFall.getPlayers().remove(skyfallPlayer);
		
		/*
		 * Destroy scoreboard
		 */
		NMSScoreboard.getScoreboard(player).destroy();
		
	}

}
