package fr.frivec.skyfall.menus;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import fr.frivec.api.spigot.item.ItemCreator;
import fr.frivec.api.spigot.menus.AbstractMenu;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.kits.KitList;
import fr.frivec.skyfall.kits.model.Kit;
import fr.frivec.skyfall.player.SkyfallPlayer;

import static fr.frivec.api.core.string.StringUtils.text;

public class KitMenu extends AbstractMenu {

	public KitMenu() {
	
		super(SkyFall.getInstance(), "§6Kits", 9*1);
	
	}

	@Override
	public void onOpen(Player player) {
		
		final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
		int slot = 0;
		
		for(KitList list : KitList.values()) {
			
			final Kit kit = list.getKit();
			final ItemCreator item = new ItemCreator(kit.getIcon(), 1).setDisplayName(kit.getName()).setLores(kit.getDescription());
			
			if(skyfallPlayer.getKit() != null && skyfallPlayer.getKit().equals(kit))
				
				item.addEnchantment(Enchantment.ARROW_DAMAGE, 1) //Add enchantment effect on the item
				.addItemFlag(ItemFlag.HIDE_ATTRIBUTES); //Hide enchantments
			
			this.addItem(this.addTag(item.build(), kit.getName().toUpperCase()), slot);
			
			slot++;
			
		}
		
	}

	@Override
	public void onClose(Player player) {/*Not used*/}

	@Override
	public void onInteract(Player player, ItemStack itemStack, int slot, InventoryAction action) {
		
		final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
		
		for(KitList kit : KitList.values()) {
			
			if(this.hasTag(itemStack, kit.getKit().getName().toUpperCase())) {
				
				if(skyfallPlayer.getKit() == null || !skyfallPlayer.getKit().equals(kit.getKit())) {
					
					skyfallPlayer.setKit(kit.getKit());
					
					player.sendMessage(text("§6Vous avez sélectionné le kit: " + kit.getKit().getName()));
					
				}else
					
					player.sendMessage(text("§cVous avez déjà sélectionné ce kit."));
				
			}
			
		}
		
		player.closeInventory();
		
	}
	
}
