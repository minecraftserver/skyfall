package fr.frivec.skyfall.teams;

import fr.frivec.api.spigot.effects.gloweffect.GlowEffect.EffectColor;
import fr.frivec.api.spigot.tags.NameTagProperty;
import fr.frivec.api.spigot.teams.Team;
import fr.frivec.skyfall.SkyFall;
import net.kyori.adventure.text.format.NamedTextColor;

public enum Teams {
	
	SPECTATOR(0, new Team("Spectator", new NameTagProperty("9Z", "§7 ", "", NamedTextColor.GRAY, EffectColor.GRAY))),
	TEAM_1(1, new Team("Team_1", new NameTagProperty("1", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_2(2, new Team("Team_1", new NameTagProperty("2", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_3(3, new Team("Team_1", new NameTagProperty("3", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_4(4, new Team("Team_1", new NameTagProperty("4", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_5(5, new Team("Team_1", new NameTagProperty("5", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_6(6, new Team("Team_1", new NameTagProperty("6", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_7(7, new Team("Team_1", new NameTagProperty("7", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_8(8, new Team("Team_1", new NameTagProperty("8", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_9(9, new Team("Team_1", new NameTagProperty("9", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_10(10, new Team("Team_1", new NameTagProperty("91", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_11(11, new Team("Team_1", new NameTagProperty("92", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED))),
	TEAM_12(12, new Team("Team_1", new NameTagProperty("93", "§c✘ ", "", NamedTextColor.RED, EffectColor.RED)));
	
	private int id;
	private Team team;
	
	private Teams(final int id, final Team team) {
		
		this.id = id;
		this.team = team;
		
		SkyFall.getInstance().getTeamManager().registerTeam(team);
		
	}
	
	public static Teams getById(final int id) {
		
		for(Teams teams : Teams.values())
			
			if(teams.getId() == id)
				
				return teams;
		
		return null;
		
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Team getTeam() {
		return team;
	}
	
	public void setTeam(Team team) {
		this.team = team;
	}

}
