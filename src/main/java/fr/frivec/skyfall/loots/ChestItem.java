package fr.frivec.skyfall.loots;

import org.bukkit.inventory.ItemStack;

public class ChestItem {
	
	private double chance;
	private ItemStack item;
	private int maxApparition; //Max amount of item in one chest
	
	public ChestItem(final double chance, final int maxApparition, final ItemStack item) {
		
		this.chance = chance;
		this.maxApparition = maxApparition;
		this.item = item;
	
	}

	public double getChance() {
		return chance;
	}

	public void setChance(double chance) {
		this.chance = chance;
	}
	
	public int getMaxApparition() {
		return maxApparition;
	}
	
	public void setMaxApparition(int maxApparition) {
		this.maxApparition = maxApparition;
	}

	public ItemStack getItem() {
		return item;
	}

	public void setItem(ItemStack item) {
		this.item = item;
	}

}
