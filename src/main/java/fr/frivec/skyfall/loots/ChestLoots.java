package fr.frivec.skyfall.loots;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import fr.frivec.api.spigot.item.ItemCreator;
import fr.frivec.skyfall.world.Island;

public class ChestLoots {
	
	//Lootable items with % of chance of apparition
	private HashSet<ChestItem> startsItems,
									intermediateItems,
									centerItems;
	
	private Random random;
	
	public ChestLoots() {
		
		this.startsItems = new HashSet<>();
		this.intermediateItems = new HashSet<>();
		this.centerItems = new HashSet<>();
		
		this.random = new Random();
		
		/*
		 * Starts items
		 */
		
		addItem(0, 20.0, 2, new ItemCreator(Material.OAK_PLANKS, 16).build());
		addItem(0, 25.0, 3, new ItemCreator(Material.STONE, 20).build());
		addItem(0, 10.0, 1, new ItemCreator(Material.STONE_SWORD, 1).build());
		addItem(0, 15.0, 1, new ItemCreator(Material.GOLDEN_SWORD, 1).build());
		addItem(0, 7.5, 1, new ItemCreator(Material.CHAINMAIL_LEGGINGS, 1).build());
		addItem(0, 7.0, 1, new ItemCreator(Material.LEATHER_CHESTPLATE, 1).build());
		addItem(0, 3.0, 1, new ItemCreator(Material.CHAINMAIL_CHESTPLATE, 1).build());
		addItem(0, 12.5, 1, new ItemCreator(Material.GOLDEN_HELMET, 1).build());
		addItem(0, 0.5, 1, new ItemCreator(Material.DEAD_BUSH, 1).setDisplayName("§aC'est ton jour de chance ! Ou pas..").setLores("§3§o0.5% de chance quand même... c'est pas rien.").build());
		addItem(0, 10.0, 2, new ItemCreator(Material.COOKED_BEEF, 16).build());	
		addItem(0, 1.5, 1, new ItemCreator(Material.STICK, 1).setDisplayName("§3Obscure référence").setLores("Plus qu'un arc et un harpon et ça le fait.").addEnchantment(Enchantment.KNOCKBACK, 1).build());
		addItem(0, 25.0, 2, new ItemCreator(Material.STONE_PICKAXE, 1).build());
		addItem(0, 10.0, 1, new ItemCreator(Material.GOLDEN_PICKAXE, 1).build());
		
		/*
		 * Intermediates islands items
		 */
		
		addItem(1, 10.0, 1, new ItemCreator(Material.OAK_PLANKS, 16).build());
		addItem(1, 12.5, 2, new ItemCreator(Material.STONE, 20).build());
		addItem(1, 4.5, 1, new ItemCreator(Material.IRON_SWORD, 1).build());
		addItem(1, 5.0, 1, new ItemCreator(Material.IRON_HELMET, 1).build());
		addItem(1, 8.5, 1, new ItemCreator(Material.CHAINMAIL_HELMET, 1).build());
		addItem(1, 8.0, 1, new ItemCreator(Material.CHAINMAIL_BOOTS, 1).build());
		addItem(1, 10.0, 1, new ItemCreator(Material.CHAINMAIL_CHESTPLATE, 1).build());
		addItem(1, 4.5, 1, new ItemCreator(Material.IRON_CHESTPLATE, 1).build());
		addItem(1, 0.5, 1, new ItemCreator(Material.DEAD_BUSH, 1).setDisplayName("§aC'est ton jour de chance ! Ou pas..").setLores("§3§o0.5% de chance quand même... c'est pas rien.").build());
		addItem(1, 8.5, 2, new ItemCreator(Material.COOKED_BEEF, 8).build());
		addItem(1, 2.5, 1, new ItemCreator(Material.FLINT_AND_STEEL, 1).build());
		addItem(1, 2.5, 3, new ItemCreator(Material.GOLDEN_APPLE, 1).build());
		addItem(1, 1.3, 1, new ItemCreator(Material.ENDER_PEARL, 1).build());
		addItem(1, 15.0, 1, new ItemCreator(Material.GOLDEN_PICKAXE, 1).build());
		addItem(1, 12.5, 1, new ItemCreator(Material.IRON_PICKAXE, 1).build());
		addItem(1, 10.0, 2, new ItemCreator(Material.ARROW, 2).build());
		
		/*
		 * Center items
		 */
		
		addItem(2, 10.0, 2, new ItemCreator(Material.BOW, 1).build());
		addItem(2, 15.0, 5, new ItemCreator(Material.ARROW, 2).build());
		addItem(2, 5.0, 1, new ItemCreator(Material.DIAMOND_SWORD, 1).build());
		addItem(2, 20.0, 1, new ItemCreator(Material.IRON_SWORD, 1).build());
		addItem(2, 10.0, 1, new ItemCreator(Material.DIAMOND_SWORD, 1).build());
		addItem(2, 7.5, 1, new ItemCreator(Material.DIAMOND_HELMET, 1).build());
		addItem(2, 10.5, 1, new ItemCreator(Material.IRON_LEGGINGS, 1).build());
		addItem(2, 10.5, 1, new ItemCreator(Material.IRON_BOOTS, 1).build());
		addItem(2, 5.0, 3, new ItemCreator(Material.GOLDEN_APPLE, 1).build());
		addItem(2, 0.5, 1, new ItemCreator(Material.DEAD_BUSH, 1).setDisplayName("§aC'est ton jour de chance ! Ou pas..").setLores("§3§o0.5% de chance quand même... c'est pas rien.").build());
		addItem(2, 10.0, 2, new ItemCreator(Material.COOKED_BEEF, 16).build());
		addItem(2, 1.5, 1, new ItemCreator(Material.STICK, 1).setDisplayName("§3Obscure référence").setLores("Plus qu'un arc et un harpon et ça le fait.").addEnchantment(Enchantment.KNOCKBACK, 1).build());
		
	}
	
	public ArrayList<ItemStack> getLoots(final Island island, final int level) {
		
		final ArrayList<ItemStack> loots = new ArrayList<>();
		final int amountItems = this.random.nextInt(5, 8);
		
		final HashSet<ChestItem> items = (level == 0 ? this.startsItems : level == 1 ? this.intermediateItems : this.centerItems);
		final double maxBound = sumOfAllChances(items);
				
		for(int turn = 0; turn < amountItems; turn++) {

			final double chance = this.random.nextDouble(0, maxBound);
						
			int i = 0;
			
			for(ChestItem chestItem : items) {
				
				final double sumPrevious = percentsSumOfPrevious(items, i),
							sumWithItem = (i + 1 >= items.size() ? sumOfAllChances(items) : percentsSumOfPrevious(items, i + 1));
								
				if(chance > sumPrevious && chance <= sumWithItem && amountOfItem(chestItem, island.getTotalLoot()) < chestItem.getMaxApparition()) {
										
					loots.add(chestItem.getItem());
					island.getTotalLoot().add(chestItem.getItem());
					break;
					
				}
				
				i++;
					
			}
			
		}
		
		return loots;
		
	}
	
	private int amountOfItem(final ChestItem item, final ArrayList<ItemStack> currentLoots) {
		
		int amount = 0;
		
		if(currentLoots.contains(item.getItem()))
			
			for(ItemStack itemStack : currentLoots)
				
				if(itemStack.equals(item.getItem()))
					
					amount++;
		
		return amount;
		
	}
	
	/**
	 * Add an item to the chest loots table of a layer.
	 * @param level: Layer of the chest loot. Put 0 if you want starts islands, 1 for intermediate islands and finally 2 for ccenter island.
	 * @param percentage: Percentage of apparition of the item in the chest
	 * @param item: Item you want to appears in chest.
	 */
	public void addItem(final int level, final double percentage, final int maxAmount, final ItemStack item) {
		
		if(level == 0)
			
			this.startsItems.add(new ChestItem(percentage, maxAmount, item));
			
		else if(level == 1)
			
			this.intermediateItems.add(new ChestItem(percentage, maxAmount, item));
		
		else if(level == 2)
			
			this.centerItems.add(new ChestItem(percentage, maxAmount, item));
			
		else
			
			throw new IllegalArgumentException("Unexpected value: " + level);
				
	}
	
	private double sumOfAllChances(final HashSet<ChestItem> items) {
		
		double sum = 0.0d;
		
		for(ChestItem item : items)
			
			sum += item.getChance();
		
		return sum;
		
	}
	
	/**
	 * Return the sum of all percents before the index
	 * @param items
	 * @param index
	 * @return
	 */
	private double percentsSumOfPrevious(final HashSet<ChestItem> items, final int index) {
		
		if(index >= items.size())
			
			return -1.0D;
		
		double sum = 0.0d;
		int i = 0;
		
		for(ChestItem item : items) {
						
			if(i == index)
				
				break;
			
			sum += item.getChance();
			
			i++;
					
		}
		
		return sum;
		
	}
	
	public HashSet<ChestItem> getStartsItems() {
		return startsItems;
	}

}
