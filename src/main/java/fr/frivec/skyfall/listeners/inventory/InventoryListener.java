package fr.frivec.skyfall.listeners.inventory;

import static fr.frivec.api.core.string.StringUtils.text;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.item.ItemCreator;
import fr.frivec.api.spigot.title.ActionBar;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.menus.KitMenu;
import fr.frivec.skyfall.player.SkyfallPlayer;
import fr.frivec.skyfall.runnables.LobbyRunnable;
import fr.frivec.skyfall.utils.States;

public class InventoryListener implements Listener {
	
	@EventHandler
	public void onDropItem(final PlayerDropItemEvent event) {
		
		if(SkyFall.getInstance().getCurrentState().equals(States.WAIT))
			
			event.setCancelled(true);
		
	}
	
	@EventHandler
	public void onInteractInventory(final InventoryClickEvent event) {
		
		final States currentState = SkyFall.getInstance().getCurrentState();
		
		if(currentState.equals(States.WAIT) || currentState.equals(States.END))
		
			event.setCancelled(true);
		
	}
	
	@EventHandler
	public void onClickItem(final PlayerInteractEvent event) {
		
		final Player player = event.getPlayer();
		final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
		final LocalPlayer localPlayer = LocalPlayer.getLocalPlayer(player.getUniqueId());
		final ItemStack item = event.getItem();
		final NamespacedKey key = new NamespacedKey(SpigotAPI.getInstance(), "LOBBY_ITEM");
		
		if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			
			if(item == null || item.getType().equals(Material.AIR))
				
				return;
			
			if(ItemCreator.hasTag(item, key, "HUB")) {
				
				localPlayer.sendToServer(null, "§aRetour au hub...");
				
				return;
				
			}else if(ItemCreator.hasTag(item, key, "KITS")) {
				
				new KitMenu().openMenu(player);
				
				return;
				
			}else if(ItemCreator.hasTag(item, key, "VOTE")) {
				
				final ItemStack startItem = new ItemCreator(Material.SLIME_BALL, 1).setDisplayName("§aCommencer la partie").setLores("§7Vous votez pour lancer la partie", "§7sans qu'elle n'est suffisamment", "§7de joueurs pour", "§7démarrer automatiquement.", "", "§3§oNécessite que tous les joueurs", "§3§odprésents votent pour le démarrage.").addTag("LOBBY_ITEM", "VOTE").build(),
						cancelVote = new ItemCreator(Material.MAGMA_CREAM, 1).setDisplayName("§cAnnuler le vote").setLores("§7Vous votez pour lancer la partie", "§7sans qu'elle n'est suffisamment", "§7de joueurs pour", "§7démarrer automatiquement.", "", "§3§oNécessite que tous les joueurs", "§3§odprésents votent pour le démarrage.").addTag("LOBBY_ITEM", "VOTE").build();;
				
				if(!skyfallPlayer.isVoteStart())
					
					ActionBar.sendActionBarToAllPlayer(text("§b" + player.getName() + " §ea voté pour que la partie commence plus tôt."));
					
				else
					
					ActionBar.sendActionBarToAllPlayer(text("§b" + player.getName() + " §ca annulé son vote."));
				
				skyfallPlayer.setVoteStart(!skyfallPlayer.isVoteStart());
				
				player.getInventory().setItem(0, skyfallPlayer.isVoteStart() ? cancelVote : startItem); //Replace item
				
				LobbyRunnable.checkVoteStart();
				
				return;
				
			}
				
		}
		
	}

}
