package fr.frivec.skyfall.listeners.players.movement;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import fr.frivec.api.spigot.scoreboard.ScoreboardPattern;
import fr.frivec.api.spigot.scoreboard.client.NMSScoreboard;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.player.SkyfallPlayer;
import fr.frivec.skyfall.utils.States;

public class PlayerMoveListener implements Listener {
	
	@EventHandler
	public void onMove(final PlayerMoveEvent event) {
		
		final SkyFall skyFall = SkyFall.getInstance();
		final Player player = event.getPlayer();
		final Location location = player.getLocation();
		
		final States currentState = skyFall.getCurrentState();
		
		/*
		 * Set the connect listener according to the current state of the game
		 */
		switch (currentState) {
		
		case WAIT: {
			
			if(location.getY() <= 50)
				
				player.teleport(new Location(Bukkit.getWorld("world"), 0.5, 60.5, 0.5, -90.0f, 0.0f));
			
			break;
		
		}
		
		case GAME: {
			
			final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
			
			if(location.getY() <= -30 && skyfallPlayer != null && !skyfallPlayer.isSpectating())
				
				player.damage(100.0); //Kill the player instantly if it's in the void
			
			/*
			 * Update scoreboard for player
			 */
			
			final Location center = SkyFall.getInstance().getWorldBuilder().getIslands(-1, false).get(0).getCenter();
			final ScoreboardPattern pattern = SkyFall.getInstance().getGamePattern();
			
			pattern.setLine(10, "§dCentre ➭ §f" + distanceBetween(location, center));
			
			NMSScoreboard.getScoreboard(player).setPattern(pattern);
			NMSScoreboard.getScoreboard(player).refreshScoreboard();
			
			break;
			
		}
		
		case END: {
			
			break;
			
		}
		
		default:
			throw new IllegalArgumentException("Unexpected value: " + currentState);
		}
		
	}
	
	/**
	 * Calculation between 2 points. Calculation with only X and Z coordinates
	 * El famoso théorème de Pythagore
	 * @param startLocation: First point
	 * @param endLocation: Second point
	 * @return a double with the distance between the two points
	 */
	private double distanceBetween(Location startLocation, Location endLocation) {
		
		double result = 0.0d;
		
		result = Math.round(Math.sqrt(Math.pow(endLocation.getZ() - startLocation.getZ(), 2) + Math.pow(endLocation.getX() - startLocation.getX(), 2)));
		
		return result;
	}

}
