package fr.frivec.skyfall.listeners.players.spectator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.craftbukkit.v1_18_R2.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import fr.frivec.api.core.player.LocalPlayer;
import fr.frivec.api.core.ranks.RankList;
import fr.frivec.api.spigot.packets.PacketUtils;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.player.SkyfallPlayer;
import net.minecraft.network.protocol.game.PacketPlayOutCamera;
import net.minecraft.world.level.World;

public class SpectatorActionsListener implements Listener {
	
	@EventHandler
	public void onInteract(final PlayerInteractEvent event) {
		
		final Player player = event.getPlayer();
		final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
		final Action action = event.getAction();
		
		if(skyfallPlayer != null && skyfallPlayer.isSpectating() && player.getGameMode().equals(GameMode.SPECTATOR)) {
			
			final HashSet<SkyfallPlayer> players = SkyFall.getInstance().getPlayers();
			final UUID playerSpectating = skyfallPlayer.getPlayerSpectating();
			
			SkyfallPlayer target = null;
			
			//if player right click, previous player, else if left click camera to next player
			if(action.equals(Action.LEFT_CLICK_AIR) || action.equals(Action.LEFT_CLICK_BLOCK))
				
				target = getPlayerInList(players, playerSpectating, true);
			
			if(target != null) {
				
				final World world = ((CraftWorld) player.getWorld()).getHandle();
				
				final PacketPlayOutCamera packet = new PacketPlayOutCamera(world.a(target.getPlayer().getEntityId()));
				
				PacketUtils.sendPacket(player, packet);
				
				player.setSpectatorTarget(target.getPlayer());
				
			}
			
		}
		
	}
	
	@EventHandler
	public void onShift(final PlayerToggleSneakEvent event) {
		
		final Player player = event.getPlayer();
		final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
		
		if(player.getGameMode().equals(GameMode.SPECTATOR) && skyfallPlayer.isSpectating()) {
			
			if(skyfallPlayer.getLocalPlayer().getRank().getPower() >= RankList.MOD.getPower())
				
				return;
			
			else {
				
				event.setCancelled(true);
				
				final HashSet<SkyfallPlayer> players = SkyFall.getInstance().getPlayers();
				final UUID playerSpectating = skyfallPlayer.getPlayerSpectating();
				
				SkyfallPlayer target = null;
				
				target = getPlayerInList(players, playerSpectating, false);
				
				if(target != null) {
				
					final World world = ((CraftWorld) player.getWorld()).getHandle();
				
					final PacketPlayOutCamera packet = new PacketPlayOutCamera(world.a(target.getPlayer().getEntityId()));
				
					PacketUtils.sendPacket(player, packet);
				
					player.setSpectatorTarget(target.getPlayer());
				
				}
				
			}
			
		}
		
	}
	
	@EventHandler
	public void playerQuitOtherCamera(final PlayerTeleportEvent event) {
		
		final Player player = event.getPlayer();
		final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
		final LocalPlayer localPlayer = skyfallPlayer.getLocalPlayer();
		
		if(skyfallPlayer != null && skyfallPlayer.isSpectating()) {
			
			if(localPlayer != null && localPlayer.getRank().getPower() >= RankList.MOD.getPower())
				
				return;
			
			if(event.getCause().equals(TeleportCause.SPECTATE) && player.getSpectatorTarget() != null)
				
				event.setCancelled(true);
			
		}
		
	}
	
	/**
	 * Get Next or previous player in list.
	 * @param players: List of all the players in the server
	 * @param spectatingID: Current spectating id of the player
	 * @param next: True if you want the next player in list, false if you want the previous one
	 * @return UUID of the next/previous player in game. Return null if no player has been found
	 */
	private SkyfallPlayer getPlayerInList(final HashSet<SkyfallPlayer> players, final UUID spectatingUUID, final boolean next) {
		
		final List<SkyfallPlayer> list = new ArrayList<>();
		SkyfallPlayer target = null;
		
		int index = -1, listIndex = 0;
		
		for(SkyfallPlayer player : players) {
			
			if(!player.isSpectating())
		
				list.add(player);
			
			if(spectatingUUID != null && spectatingUUID.equals(player.getPlayer().getUniqueId()))
				
				index = listIndex;
				
			listIndex++;
				
		}
		
		if(!next) {
			
			list.sort(Collections.reverseOrder());
			
			index = index - (list.size() + 1); //Reverse the index. Minus size + 1 because the list doesn't start à 1 but at 0
			
		}
		
		index++; //Get next or previous player in list
		
		if(index < list.size() && index >= 0)
			
			target = list.get(index);
		
		return target;
		
	}

}
