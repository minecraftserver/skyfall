package fr.frivec.skyfall.listeners.players.damages;

import static fr.frivec.api.core.string.StringUtils.text;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_18_R2.CraftWorld;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.persistence.PersistentDataType;

import fr.frivec.api.spigot.packets.PacketUtils;
import fr.frivec.api.spigot.scoreboard.client.NMSScoreboard;
import fr.frivec.api.spigot.title.ActionBar;
import fr.frivec.api.spigot.title.Title;
import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.player.SkyfallPlayer;
import fr.frivec.skyfall.utils.States;
import net.minecraft.network.protocol.game.PacketPlayOutCamera;
import net.minecraft.world.level.World;

public class PlayerDamageListener implements Listener {
	
	private final NamespacedKey key = new NamespacedKey(SkyFall.getInstance(), "LAST_ATTACKER");
	
	@EventHandler
	public void onDeath(final PlayerDeathEvent event) {
		
		Player player = event.getEntity(),
						killer = player.getKiller();
		final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
		
		if(skyfallPlayer != null && !skyfallPlayer.isSpectating()) {
			
			event.setCancelled(true);
			
			String attackerName = null;
			
			//Set player in game mode spectator
			player.setGameMode(GameMode.SPECTATOR);
			
			if(player.getPersistentDataContainer().has(this.key))
				
				attackerName = player.getPersistentDataContainer().get(this.key, PersistentDataType.STRING);
			
			if(attackerName != null && killer == null)
				
				killer = Bukkit.getPlayer(attackerName);
			
			if(killer != null)
				
				event.deathMessage(text("§c" + player.getName() + " §3a été tué(e) par §6" + killer.getName() + "§3."));
				
			else
				
				event.deathMessage(text("§c" + player.getName() + " §3est mort(e)."));
			
			//Send sound to killed player
			player.playSound(player, Sound.ENTITY_GHAST_HURT, 1.0f, 0.5f);
			
			//Update killer statistics
			
			if(killer != null) {
				
				//Send message and sound to killer
				ActionBar.sendActionBarToPlayer(killer, text("§aVous avez tué §c" + player.getName()));					
				killer.playSound(killer, Sound.BLOCK_NOTE_BLOCK_PLING, 1.0f, 0.5f);
			
				SkyfallPlayer.getPlayer(killer.getUniqueId()).getGameStats().increaseKills();
				
				//Update Scoreboard
				final NMSScoreboard killerScoreboard = NMSScoreboard.getScoreboard(killer);
				
				killerScoreboard.getPattern().setLine(8, "§aTué(s) ➭  §f" + SkyfallPlayer.getPlayer(killer.getUniqueId()).getGameStats().getKills());
				killerScoreboard.refreshScoreboard();
				
				//Set the killer's uuid as spectating player for the dead player
				skyfallPlayer.setPlayerSpectating(killer.getUniqueId());
				
			}
			
			//Update assists statistics
			for(UUID assist : skyfallPlayer.getAssists())
				
				if(killer != null && !killer.getUniqueId().equals(assist)) //Check if the killer exists and if yes, do not add assist to the killer.
				
					SkyfallPlayer.getPlayer(assist).getGameStats().increaseAssits();
			
			//Update death stat of player
			skyfallPlayer.getGameStats().increaseDeaths();
			
			//Update amount of players
			SkyFall.getInstance().setAmountPlayers(SkyFall.getInstance().getAmountPlayers() - 1);
			
			//Update scoreboard for remaining players
			for(SkyfallPlayer players : SkyFall.getInstance().getPlayers()) {
				
				final NMSScoreboard playerScoreboard = NMSScoreboard.getScoreboard(players.getPlayer());
				
				playerScoreboard.getPattern().setLine(6, "§6Joueur(s) en vie ➭ §f" + SkyFall.getInstance().getAmountPlayers());
				playerScoreboard.refreshScoreboard();
				
			}
			
			//Send title to the player
			Title.sendTitleToPlayer(player, "§cVous êtes mort", "§bFin de la partie !", 1000, 2000, 1000);
			
			//Send tutorial to player
			player.sendMessage(text("§aLa partie est terminée !"));
			player.sendMessage(text("§bVous êtes à présent en mode spectateur. Vous pouvez revenir au hub à tout moment avec §a/hub §b!"));
			player.sendMessage(text(""));
			player.sendMessage(text("§6Astuce: Vous pouvez voir la vue de chaque joueur."));
			player.sendMessage(text("§6Pour cela, faites §aClic-gauche §6pour aller au joueur suivant et §aClic-droit pour aller au joueur précédent."));
			
			//If the killer isn't null, set the player's camera to the killer's one
			if(killer != null)
				
				setPlayerCamera(player, killer);
			
			else {
				
				SkyfallPlayer newCamera = null;
			
				//Search for the first player in the list which is player
				for(SkyfallPlayer playing : SkyFall.getInstance().getPlayers()) {
					
					if(!playing.isSpectating()) {
						
						newCamera = playing;
						
						skyfallPlayer.setPlayerSpectating(playing.getUuid());
						
						break;
						
					}
					
				}
				
				if(newCamera != null)
				
					setPlayerCamera(player, newCamera.getPlayer()); //Set the camera to the first player still alive
				
				else
					
					player.teleport(new Location(player.getWorld(), 0.5, 15.0, 0.5, 0.0f, 90.0f)); //If all the players are dead, teleport him to the center
				
			}
			
			/*
			 * Set player into spectator team
			 */
			
			
			
		}else
			
			player.sendMessage(text("§cIl s'est passé quoi là ?! Comment t'es mort ?"));
		
	}
	
	@EventHandler
	public void onPlayerAttackPlayer(final EntityDamageByEntityEvent event) {
		
		if(event.getEntity().getType().equals(EntityType.PLAYER) && event.getDamager().getType().equals(EntityType.PLAYER)) {
			
			final Player player = (Player) event.getEntity(),
							damager = (Player) event.getDamager();
			
			final SkyfallPlayer skyfallPlayer = SkyfallPlayer.getPlayer(player.getUniqueId());
			
			//Add assists
			skyfallPlayer.getAssists().add(damager.getUniqueId());
			
			//Set last attacker
			player.getPersistentDataContainer().set(this.key, PersistentDataType.STRING, damager.getName());
			
		}
		
	}
	
	@EventHandler
	public void onDamage(final EntityDamageEvent event) {
		
		final States currentState = SkyFall.getInstance().getCurrentState();
		
		if(currentState.equals(States.WAIT) || currentState.equals(States.END)) {
			
			if(currentState.equals(States.END) && event.getCause().equals(DamageCause.VOID))
				
				event.getEntity().teleport(new Location(Bukkit.getWorld("world"), 0, 0.5, 0));
			
			event.setCancelled(true);
			
		}
		
	}
	
	private void setPlayerCamera(final Player player, final Player newCamera) {
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(SkyFall.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				
				final World world = ((CraftWorld) player.getWorld()).getHandle();
				
				final PacketPlayOutCamera packet = new PacketPlayOutCamera(world.a(newCamera.getEntityId()));
				
				PacketUtils.sendPacket(player, packet);
				
				player.setSpectatorTarget(newCamera);
				
			}
			
		}, 5L);
		
	}

}
