package fr.frivec.skyfall.listeners.chest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDropItemEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.frivec.skyfall.SkyFall;
import fr.frivec.skyfall.player.SkyfallPlayer;
import fr.frivec.skyfall.utils.States;

public class ChestOpeningListener implements Listener {
	
	/**
	 * Event targeted when a block drops items.
	 * Used to drop naturally items of the chest on the ground
	 * 
	 */
	@EventHandler
	public void onChestDropsItems(final BlockDropItemEvent event) {
		
		final Block block = event.getBlock();
		
		if(block.getType().equals(Material.CHEST) && SkyFall.getInstance().getCurrentState().equals(States.GAME)) {
			
			final Iterator<Item> drops = event.getItems().iterator();
			
			while(drops.hasNext())
				
				if(drops.next().getItemStack().getType().equals(Material.CHEST))
					
					drops.remove();
			
		}
		
	}
	
	@EventHandler
	public void onCloseChest(final InventoryCloseEvent event) {
		
		final SkyFall skyFall = SkyFall.getInstance();
		final Inventory inventory = event.getInventory();
		final SkyfallPlayer player = SkyfallPlayer.getPlayer(event.getPlayer().getUniqueId());
		
		//Check if the game is currently running
		if(inventory.getType().equals(InventoryType.CHEST) && skyFall.getCurrentState().equals(States.GAME)) {
			
			if(player.isSpectating())
				
				return;
			
			final Chest chest = (Chest) inventory.getHolder();
			final Block block = chest.getBlock();
			
			//Register all items in a list to add it to the event
			final List<Item> items = new ArrayList<>();
			
			for(ItemStack stack : inventory.getContents()) {
				
				if(stack != null) {
				
					final Item item = block.getLocation().getWorld().dropItem(block.getLocation(), stack);
					
					items.add(item);
					
				}
				
			}
			
			//Call the BlockDropItemEvent to drop items naturally
			Bukkit.getServer().getPluginManager().callEvent(new BlockDropItemEvent(block, chest, (Player) event.getPlayer(), items));
			block.setType(Material.AIR);
			
		}
		
	}

}
