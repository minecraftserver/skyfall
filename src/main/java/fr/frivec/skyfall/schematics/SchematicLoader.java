package fr.frivec.skyfall.schematics;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormats;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardReader;
import com.sk89q.worldedit.function.operation.Operation;
import com.sk89q.worldedit.function.operation.Operations;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.session.ClipboardHolder;

import fr.frivec.api.core.logger.Logger.LogLevel;
import fr.frivec.api.spigot.SpigotAPI;
import fr.frivec.api.spigot.cuboid.Cuboid;
import fr.frivec.skyfall.world.Island;

public class SchematicLoader {
	
	private Path schematicFolder;
	
	private HashMap<String, Clipboard> clipboards;
	
	public SchematicLoader(final Path schematicFolder) {
		
		this.schematicFolder = schematicFolder;
		this.clipboards = new HashMap<>();
		
	}
	
	public boolean pasteSchematic(final Island island, final String schematicName) {
		
		return pasteSchematic(island, loadSchematic(schematicName));
		
	}
	
	public boolean pasteSchematic(final Location location, final String schematicName) {
		
		return pasteSchematic(location, loadSchematic(schematicName));
		
	}
	
	public boolean pasteSchematic(final Island island, final Clipboard schematic) {
		
		final Location location = island.getCenter();
		
		final Cuboid cuboid = island.getCuboid();
		
		for(Block blocks : cuboid)
			
			if(!blocks.getType().equals(Material.AIR))
				
				return false;
		
		try (final EditSession session = WorldEdit.getInstance().newEditSession(new BukkitWorld(Bukkit.getWorld("world")))) {
			
			final Operation operation = new ClipboardHolder(schematic)
											.createPaste(session)
											.to(BlockVector3.at(location.getX(), location.getY(), location.getZ()))
											.ignoreAirBlocks(true)
											.copyBiomes(true)
											.copyEntities(true)
											.build();
			
			Operations.complete(operation);
			
			session.close();
			
			return true;
			
		} catch (WorldEditException e) {
			
			SpigotAPI.log(LogLevel.SEVERE, "Unable to paste schematic.");
			e.printStackTrace();
			
			return false;
			
		}
		
	}
	
	/**
	 * Paste a schematic only with a location and a schematic.
	 * Do not check if the place is already used !
	 * @param location: Center of schematic
	 * @param schematic: Schematic you want to paste
	 * @return true if the schematic has been paste or false if not
	 */
	public boolean pasteSchematic(final Location location, final Clipboard schematic) {
		
		try (final EditSession session = WorldEdit.getInstance().newEditSession(new BukkitWorld(Bukkit.getWorld("world")))) {
			
			final Operation operation = new ClipboardHolder(schematic)
											.createPaste(session)
											.to(BlockVector3.at(location.getX(), location.getY(), location.getZ()))
											.ignoreAirBlocks(true)
											.copyBiomes(true)
											.copyEntities(true)
											.build();
			
			Operations.complete(operation);
			
			session.close();
			
			return true;
			
		} catch (WorldEditException e) {
			
			SpigotAPI.log(LogLevel.SEVERE, "Unable to paste schematic.");
			e.printStackTrace();
			
			return false;
			
		}
		
	}
	
	public Clipboard loadSchematic(final String schematicName) {
		
		final Path path = Paths.get(this.schematicFolder + "/" + schematicName + ".schem");
		
		Clipboard clipboard = null;
		
		if(this.clipboards.containsKey(schematicName))
			
			clipboard = this.clipboards.get(schematicName);
		
		else {
		
			if(Files.exists(path)) {
			
				final ClipboardFormat format = ClipboardFormats.findByFile(path.toFile());
				
				try (final ClipboardReader reader = format.getReader(Files.newInputStream(path))) {
					
					clipboard = reader.read();
					
					reader.close();
					
				} catch (IOException e) {
				
					SpigotAPI.log(LogLevel.WARNING, "Unable to load the schematic " + schematicName + ".schem");
					e.printStackTrace();
					
				}
				
			}
			
		}
		
		return clipboard;
		
	}

}
